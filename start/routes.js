'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('index')
Route.get('/tverify', 'ByauthyController.try_verify')
Route.get('/regi', 'ByauthyController.register')
Route.get('/del', 'ByauthyController.delete_user')
Route.get('/state', 'ByauthyController.status_user')
Route.get('/very', 'ByauthyController.verify')
Route.get('/sms', 'ByauthyController.req_sms')
Route.get('/call', 'ByauthyController.req_call')
Route.get('/phone', 'ByauthyController.phone_very')

Route.post('/', 'ByauthyController.autentication')
Route.post('/delete', 'ByauthyController.destroy')
Route.post('/code', 'ByauthyController.validation')

Route.on('/code').render('code')
Route.on('/result').render('result')

Route.post('/push', 'ByauthyController.aprobationPush')
Route.get('/status', 'ByauthyController.getResult')
Route.post('/status', 'ByauthyController.showResult')
// Route.post('/push', 'PushController.aprobation')