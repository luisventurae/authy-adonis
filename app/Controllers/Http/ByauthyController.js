'use strict'

const User = use('App/Models/User')

// npm install authy
const YOUR_AUTHY_API_KEY = "Ykr7DxonFHYr1VuuTnJ80qc1xLE1fcVE"
const authy = require("authy")(YOUR_AUTHY_API_KEY)

// credentials authy PRO
// const YOUR_AUTHY_API_KEY = "Ykr7DxonFHYr1VuuTnJ80qc1xLE1fcVE"
// const authy = require("authy")(YOUR_AUTHY_API_KEY)

/** Credential Authy Notification */
const accountSid_not = 'AC5824b7b8dacba5a7ddad92efe9bbd00b';
const authToken_not = '2177b36dc3654a797c3f91842200641e';
const client_not = require('twilio')(accountSid_not, authToken_not);

class ByauthyController {
    async try_verify({view}){
        authy
            .phones()
            .verification_start("+51982130528", "1", "sms", function(err, res) {
                if (err) {
                    console.log('ERROR D:')
                    console.log(err) 
                }
                console.log('El mensaje:')
                console.log(res.message)
            })
        return view.render('welcome', {msg: "Authy Verify - Ejecutado"})
    }

    async register({view}) {
        authy.register_user('james_lo_max@hotmail.com', '997436528', '51', function (err, res) {
            // res = {user: {id: 1337}} where 1337 = ID given to use, store this someplace
            console.log(`${res.user.id}`)
        })
        return view.render('welcome', {msg: "Authy Verify - Registrando usuario"})
    }

    async verify({view}) {
        authy.verify('124490757', '0000000', function (err, res) {
            // console.log(`${res.user}`)
        })
        return view.render('welcome', {msg: "Authy Verify - verificando usuario"})
    }

    async req_sms({view}) {
        authy.request_sms('124490757', function (err, res) {
            // console.log(`${res.user}`)
        })
        return view.render('welcome', {msg: "Authy Verify - Requiriendo token por sms"})
    }
    
    async req_call({view}) {
        authy.request_call('124490757', function (err, res) {
            // console.log(`${res.user}`)
        })
        return view.render('welcome', {msg: "Authy Verify - Requiriendo token por llamada"})
    }

    async delete_user({view}) {
        authy.delete_user('125150185', function (err, res) {
            // console.log(`${res.user}`)
        })
        return view.render('welcome', {msg: "Authy Verify - Usuario borrado"})
    }

    async status_user({view}) {
        authy.user_status('124490757', function (err, res) {
            console.log(`${res}`)
        })
        return view.render('welcome', {msg: "Authy Verify - Estado del usuario"})
    }
    
    async phone_very({view}) {
        authy.phones().verification_start('982130528', '51', { via: 'sms', locale: 'es', code_length: '6' }, function(err, res) {
 
        })
        return view.render('welcome', {msg: "Authy Verify - Verificacion del fono"})
    }

    /**
     *  Test Juntos
     * 
     */
    async autentication({request, response, view, session, auth}) {
        // var valido = false
        session.put('valido', false)
        const user = new User()

        user.correo = request.input('correo')
        user.cpais = request.input('pais')
        user.numero = request.input('fono')

        let correo = user.correo
        let cpais = user.cpais
        let numero = user.numero

        session.put('cpais', cpais)
        session.put('numero', numero)

        console.log(`${correo}`)
        console.log(`${cpais}`)
        console.log(`${numero}`)

        authy.register_user(correo, numero, cpais, (err, res) => {
            if(res.user.id){
                let idAuthy = res.user.id
                console.log(`ID ${idAuthy}`)
                authy.phones().verification_start(numero, cpais, { via: 'sms', locale: 'es', code_length: '6' }, function(err, req, resp) {
                    console.log('Codigo enviado')
                    session.put('valido', true)
                    console.log(`Valido 1 = ${session.get('valido')}`)
                    // response.writeHead(302, {"Location": "http://" + request.headers['host'] + "/code" })
                    // return response.end() 
                    // return resp.redirect('/code')
                    // return view.render('code')
                    // return
                })
            }else{
                console.log('Codigo NO enviado')
                session.put('valido', false)
                return view.render('result', {msg: "Authy Verify - Usuario no se pusdo Registrar"})
            }
        })
        // console.log(`Valido 2 = ${session.get('valido')}`)
        // if(session.get('valido')){
        //     return view.render('code')
        // }else{
        //     return view.render('result', {msg: "Authy Verify - Usuario no se pusdo Registrar"})
        // }
    }
    
    async validation({request, response, view, session, auth}) {
        let codigo = request.input('codigo')
        
        authy.phones().verification_check(session.get('numero'), session.get('cpais'), codigo, function (err, res) {
            console.log(`Verification ${res}`)
            if(res){
                console.log('redirect result')
                return res.route('/result')
                return view.render('result', {msg: "Se ha verificado el número"})
            }else{
                console.log('render code')
                return view.render('code', {msg: "Código invalido"})
            }
        })
        
        // authy.phones().verification_check(auth.user.numero, auth.user.cpais, codigo, function (err, res) {
        //     console.log(`${res}`)
        // })
    }

    async destroy({request, response, view}) {
        let idAuthy = request.input('idAuthy')
        authy.delete_user(idAuthy, function (err, res) {
            console.log('Eliminado correctamente')
        })
        return view.render('result', {msg: "Authy Verify - Usuario borrado"})
    }
    
    async aprobationPush({response}) {
        let id = '124490757'
        let user_payload = {message:'<h1>S/. 20</h1>\nPuku Puku cafe de Lucio<br>\nAcepta para pagar ahora', seconds_to_expire: 60, 'details':{Usuario:'Luis Ventura', Ubicacion:'Lima, Peru', Telefono:'+51982130528', Negocio:'Puku Puku Lucio'}}
        let hidden_details = {transaction_num:'AAA000123'}
        let logos = { '':{res:'default', url:'https://i.ibb.co/dGpw5pV/Group.png'}}
        
        authy.send_approval_request(id, user_payload, hidden_details, logos, function (err, res) {
            // res = {"approval_request":{"uuid":"########-####-####-####-############"},"success":true}
            console.log('Imprimiendo Logo'+logos)
            console.error(logos)
            if(res){
                console.log('Supuestamente enviado :u + ' + res )
                console.error(res)
                let uuid = res['approval_request']['uuid']
                console.log(uuid)
            }else{
                console.log('creo q no envió -> ')
                console.error(err)
            }
        })
    }

    async getResult({view, response}) {
        return view.render('status')
    }

    async showResult({request, response, view}) {
        let uuid = request.input('uuid')
        console.log(uuid)
        authy.check_approval_status(uuid, function(err, res) {
            if(res){
                console.log('hay resultado')
                // console.error(res)
                let estado = res['approval_request']['status']
                console.log('Estado: '+estado)
                return view.render('status', {estado: estado})
            }else{
                console.log('NO hay resultado')
            }
        })
    }
}

module.exports = ByauthyController
